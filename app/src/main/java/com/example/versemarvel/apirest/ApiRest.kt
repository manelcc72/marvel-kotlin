package com.example.versemarvel.apirest

import com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities.ApiCharactersResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRest {

    /**
     * Get main api “characters” marvel
     */
    @GET("characters")
    fun getCharacters(
        @Query("ts") ts: String,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String
    ): Observable<ApiCharactersResponse>

}