package com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities

import com.google.gson.annotations.SerializedName

data class ApiComics(

	@field:SerializedName("collectionURI")
	val collectionURI: String? = null,

	@field:SerializedName("available")
	val available: Int? = null,

	@field:SerializedName("returned")
	val returned: Int? = null,

	@field:SerializedName("itemApis")
	val itemApis: List<ApiItemsItem?>? = null
)