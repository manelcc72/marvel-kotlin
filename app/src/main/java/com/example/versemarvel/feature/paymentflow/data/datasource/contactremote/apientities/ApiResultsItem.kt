package com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities

import com.google.gson.annotations.SerializedName


data class ApiResultsItem(

	@field:SerializedName("apiThumbnail")
	val apiThumbnail: ApiThumbnail? = null,

	@field:SerializedName("urls")
	val urls: List<ApiUrlsItem?>? = null,

	@field:SerializedName("apiStories")
	val apiStories: ApiStories? = null,

	@field:SerializedName("apiSeries")
	val apiSeries: ApiSeries? = null,

	@field:SerializedName("apiComics")
	val apiComics: ApiComics? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("modified")
	val modified: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("resourceURI")
	val resourceURI: String? = null,

	@field:SerializedName("apiEvents")
	val apiEvents: ApiEvents? = null
)