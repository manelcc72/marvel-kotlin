package com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities

import com.google.gson.annotations.SerializedName

data class ApiItemsItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("resourceURI")
	val resourceURI: String? = null
)