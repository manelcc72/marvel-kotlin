package com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities

import com.google.gson.annotations.SerializedName

data class ApiThumbnail(

    @field:SerializedName("path")
    val path: String? = null,

    @field:SerializedName("extension")
    val extension: String? = null
)