package com.example.versemarvel.feature.paymentflow.presenter

import com.example.versemarvel.core.base.BasePresenter
import com.example.versemarvel.core.base.BaseView

interface ContactListPresenter {

    interface Presenter : BasePresenter<View> {
        fun stopServices()

    }

    interface View : BaseView<Presenter> {


    }
}