package com.example.versemarvel.feature.paymentflow.data.datasource.contactremote.apientities

import com.google.gson.annotations.SerializedName


data class ApiUrlsItem(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)