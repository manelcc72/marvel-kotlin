package com.example.versemarvel

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MarvelVerseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MarvelVerseApplication)
            androidLogger()
            modules(
                listOf(
                   //TODO: list modules
                )
            )
        }

    }
}